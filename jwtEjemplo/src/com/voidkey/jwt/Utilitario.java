package com.voidkey.jwt;

import java.security.Key;
import java.util.Date;
import java.util.UUID;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class Utilitario {
	
	final static String fraseSecreta ="TEST";
	
	public static void  generarToken(String valor)
	{
		try {
		
		System.out.println("Se generara un token con tiempo de expiracion de "+valor+" minutos");
		String identificador =  UUID.randomUUID().toString();
		System.out.println("UUID :"+ identificador);
		
		SignatureAlgorithm algoritmo =SignatureAlgorithm.HS512;
		
		long milisec = System.currentTimeMillis();
		Date ahora = new  Date(milisec);
		
		byte[] secreto = DatatypeConverter.parseBase64Binary(fraseSecreta);
		Key llave = new SecretKeySpec(secreto,algoritmo.getJcaName());
		
		 JwtBuilder constructor = Jwts.builder().setId(identificador)
		            .setIssuedAt(ahora)
		            .setSubject("prueba@void-key.com") //correo
		            .setIssuer("void-key.com")   //
		            .signWith(algoritmo, llave);
		 
		long ttlMillis = 60000*Long.parseLong(valor);
		 if (ttlMillis > 0) {
		        long expMillis = milisec + ttlMillis;
		        Date exp = new Date(expMillis);
		        constructor.setExpiration(exp);
		    }  
		 
		 System.out.println("Token generado "+constructor.compact());
		 
		}catch(Exception e)
		{
			System.out.println("Error"+e);
		}
	}
	
	public static Claims validarToken(String jwt) {
	    
		System.out.println("Revisando token "+jwt);
		try {
		Claims claims = Jwts.parser()
	            .setSigningKey(DatatypeConverter.parseBase64Binary(fraseSecreta))
	            .parseClaimsJws(jwt).getBody();
	    
	    validarFecha(claims.getExpiration());
	    return claims;
		}catch(Exception e)
		{
			System.out.println("Token no valido");
			System.out.println("Error "+e);
			return null;
		}
	}
	
	public static boolean validarFecha(Date expiracion)
	{
		long milisec = System.currentTimeMillis();
		Date ahora = new  Date(milisec);
		
		System.out.println("Tiempo Actual [["+ahora +"]]-- Token [["+expiracion+"]]");
		if(ahora.before(expiracion))
		{
			System.out.print("LLave valida");
			return true;
		}else
		{
			System.out.println("LLave invalida");
			return false;
		}
		
	}

}
