package com.voidkey.jwt;

/**
 * 
 * @author void-key.com https://void-key.com
 */

public class Jwt extends Utilitario {

	/**
	 * 
	 * @param args primer valor es la cantidad de minutos que debe esperar segundo
	 *             valor es la accion gen o val (generacion o validacion)
	 */
	public static void main(String[] args) {

		if (args.length != 2) {
			if (!validarArgumentos(args)) {
				System.out.println("Argumentos Invalidos");
				System.exit(0);
			}
		}

		switch (args[1]) {
		case "gen":
			generarToken(args[0]);
			break;
		case "val":
			validarToken(args[0]);
			break;
		}

	}

	public static boolean validarArgumentos(String[] args) {
		if ("gen".equals(args[1])) {
			try {
				@SuppressWarnings("unused")
				int valor = Integer.parseInt(args[0]);
				return true;
			} catch (NumberFormatException e) {
				return false;
			}
		}

		if ("val".equals(args[1]))
			return true;

		return false;
	}
}
